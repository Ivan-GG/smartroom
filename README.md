## README ##

#Instalacion del Proyecto#

Despues de clonar el proyecto se veran dos carpetas, client y server.

Primero entramos en 

`cd server`

y despues

`npm install`

para instalar los modulos necesarios, despues para iniciarlo

`node server`

ya teniendo nuestro servidor, con otra consola de comandos, entramos en

`cd client`

y despues

`ng serve`

y así iniciar la aplicacion.

Hay que tener en cuenta que primero debemos tener inicializado nuestro sericio de mongodb en el servidor 21017,
ademas hay que conectar el arduino.

El usuario y la contraseña para probar el sistema es "admin".