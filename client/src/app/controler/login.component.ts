
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
 
import { User } from '../_models/user';
import { AlertService, UserService, AuthenticationService } from '../_services/index';
 
@Component({
    moduleId: module.id,
    templateUrl: '../views/login.component.html',
    styleUrls: ['../assets/login.component.css']
})
 
export class LoginComponent implements OnInit {
    model = new User('','','','','','');
    admin = new User('','admin','admin','admin','admin','0');
    loading = false;
    returnUrl: string;
    error = false;
 
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService) { }
 
    ngOnInit() {
        // reset login status
        //this.authenticationService.logout();
 
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.registrar();
    }

    private registrar()
    {   

       



        this.admin['firstName'] = "admin";
        this.admin['lastName'] = "admin";
        this.admin['password'] = "admin";
        this.admin['username'] = "admin";
        this.admin['tipo'] = "0";



        console.log(this.admin);

        this.userService.create(this.admin).subscribe(
                data => {
                    
                },
                error => {
                    
                });
    }

    cerrar()
    {
        this.error = false;
    }
 
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    //this.alertService.error(error);
                    this.loading = false;
                    this.error = true;
                });
    }
}